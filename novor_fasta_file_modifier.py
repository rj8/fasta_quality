__author__ = 'rj8'
"""Input is  the Novor output file, and a fasta file.  The Novor-derived sequences that exceed a specified sequence
score are appended to create a single "protein" sequence, which is then appended to the fasta file."""

import shutil, os

novor_file = "QEP2_2016_0512_RJ_13_human_fixed.ms2.csv"
novor_fasta_header = ">nv|000000|NOVOR_NOVOR Concatenated de novo sequences from " + novor_file + " GN=NOVOR PE=6\n"
database = "\\backup\dbase\uniprot-Fcatus_crap.fasta"
split_database = database.split('\\')
new_database = "\\backup\\novor_dbase\\" + split_database[len(split_database) - 1] + '.novorScr0.' + novor_file[:-8]
sequences = []
score_cutoff = 0 #sequence score must exceed this value in order to be subjected to a CIDentify search, default=60
min_peptide_length = 6 # minimum peptide length for evaluation by CIDentify
low_charge = 2 # lowest allowed charge state of the precursor
high_charge = 3 # highest allowed charge state of the precursor
make_file = True # True will make the modified fasta file, False will just report the number of de novo seqs

# open the searchFile and make a list of sequences
InFile = open(novor_file, 'rU')
novor_lines = InFile.readlines()
InFile.close()

# extract the sequences to search using CIDentify
header = True
for line in novor_lines:
    info = line.split(',')
    if header:
        if len(info) == 12:
            if info[0] == '# id' and info[1] == ' scanNum' and info[2] == ' RT':
                header = False
        continue
    if float(info[8]) > score_cutoff and int(info[4]) >= low_charge and int(info[4]) <= high_charge:
        c_list_1 = list(info[9].strip())
        c_list_2 = []
        include = True
        for c in c_list_1:
            if c == '(':
                include = False
                continue
            if c == ')':
                include = True
                continue
            if include:
                c_list_2.append(c)
        if len(c_list_2) >= min_peptide_length:
            new_sequence = "".join(c_list_2)
            if new_sequence not in sequences:
                sequences.append(new_sequence)

if make_file:
    print "Number of sequences appended to make NOVOR_NOVOR: " + str(len(sequences))
    novor_protein = ''.join(sequences)
    shutil.copy(database, new_database)
    OutFile = open(new_database, 'a')
    OutFile.write(novor_fasta_header)
    OutFile.write(novor_protein)
    OutFile.close()
else:
    print "Number of sequences: " + str(len(sequences))