__author__ = 'rj8'
"""This script imports an ms2 file and the resulting Novor output file.  It counts the number of total ms2 spectra, the
number of spectra assigned de novo sequences with sequence scores greater than a lower limit, and the number of
unique de novo sequences."""
import csv

ms2_filename = "QEP1_2018_0814_RJ_40_x3_fixed.ms2" #ms2 file used as input to Novor
novor_filename = "QEP1_2018_0814_RJ_40_x3_fixed.ms2.csv" #novor output file
novor_score_cutoff = 60 # Scores must be greater than or equal to this number in order to be counted

file_name_list = "input.txt" #each line is the name of a pep.xml file to be modified
output_filename = "output.csv"

# open the input file name list
InFile = open(file_name_list, 'rU')
lines = InFile.readlines()
InFile.close()
file_names = [x.strip() for x in lines]

output_dict_list = []
for ms2_filename in file_names:
    novor_filename = ms2_filename + ".csv"

    spectrum_num = 0 # Total number of spectra in the ms2 file
    count_1 = 0 # Number of good sequences that had precursors with this charge state
    count_2 = 0
    count_3 = 0
    count_4 = 0
    count_all_others = 0
    total_1 = 0 # Number of ms2 spectra with this precursor charge state
    total_2 = 0
    total_3 = 0
    total_4 = 0
    total_all_others = 0
    unique = set() # Set of unique sequences


    # Get the file info
    InFile = open(ms2_filename, 'rU')
    ms2_lines = InFile.readlines()
    InFile.close()

    InFile = open(novor_filename, 'rU')
    novor_lines = InFile.readlines()
    InFile.close()

    # Count the number of spectra in the ms2 file
    for line in ms2_lines:
        info = line.split()
        if info[0] == 'Z':
            spectrum_num += 1
            if int(info[1]) == 1:
                total_1 += 1
            elif int(info[1]) == 2:
                total_2 += 1
            elif int(info[1]) == 3:
                total_3 += 1
            elif int(info[1]) == 4:
                total_4 += 1
            else:
                total_all_others += 1


    # Count the number of spectra with high scoring de novo sequences
    header = True
    for line in novor_lines:
        info = line.split(',')
        if header:
            if len(info) == 12:
                if info[0] == '# id' and info[1] == ' scanNum' and info[2] == ' RT':
                    header = False
            continue
        if len(info) < 10: continue
        if float(info[8]) > novor_score_cutoff:
            unique.add(info[9].strip())
            if int(info[4]) == 1:
                count_1 += 1
            elif int(info[4]) == 2:
                count_2 += 1
            elif int(info[4]) == 3:
                count_3 += 1
            elif int(info[4]) == 4:
                count_4 += 1
            else:
                count_all_others += 1

    output_dict = {}
    output_dict['Filename'] = ms2_filename
    output_dict['Total ms2 spectra'] = spectrum_num
    output_dict['Total +1'] = total_1
    output_dict['Total +2'] = total_2
    output_dict['Total +3'] = total_3
    output_dict['Total +4'] = total_4
    output_dict['Total >+4'] = total_all_others
    output_dict['Total high scoring spectra'] = count_1 + count_2 + count_3 + count_4 + count_all_others
    output_dict['High scoring +1'] = count_1
    output_dict['High scoring +2'] = count_2
    output_dict['High scoring +3'] = count_3
    output_dict['High scoring +4'] = count_4
    output_dict['High scoring >+4'] = count_all_others
    output_dict['Unique de novo sequences'] = len(unique)
    output_dict_list.append(output_dict)
    print "Finished with " + ms2_filename

with open(output_filename, 'w') as csvfile:
    # input info
    spamwriter = csv.writer(csvfile, lineterminator='\n')
    csvfile.write('Novor sequence score cutoff:' + str(novor_score_cutoff) + '\n')

    # summary info
    field_names = ['Filename', 'Total ms2 spectra', 'Total +1', 'Total +2', 'Total +3', 'Total +4', 'Total >+4',
                   'Total high scoring spectra', 'High scoring +1', 'High scoring +2', 'High scoring +3',
                   'High scoring +4', 'High scoring >+4', 'Unique de novo sequences']
    dict_writer = csv.DictWriter(csvfile, lineterminator='\n', fieldnames=field_names)
    dict_writer.writeheader()
    dict_writer.writerows(output_dict_list)