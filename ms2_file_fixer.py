__author__ = 'rj8'
"""Extract m/z data from both mgf and ms2 files that were derived from the same raw file, and compare the peptide MWs.
Output will be lists of scan numbers, retention times, charges and m/z and peptide MWs derived from the two formats."""
scan_num = 0 #only used to count the number of spectra to report at the end in a print statement
import random
randRange = [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
randomize_precursor = False
randomize_fragments = False
unchanged_prob = 1.0 #probability that a fragment ion will not be scrambled

file_name_list = "input.txt" #each line is the name of a pep.xml file to be modified
# open the input file name list
InFile = open(file_name_list, 'rU')
lines = InFile.readlines()
InFile.close()
file_names = [x.strip() for x in lines]

for ms2_file in file_names:
    scan_num = 0  # only used to count the number of spectra to report at the end in a print statement
    if randomize_precursor and randomize_fragments:
        output_file = ms2_file[:-4] + '_randPrecFrag.ms2'
    elif randomize_precursor and not randomize_fragments:
        output_file = ms2_file[:-4] + '_randPrec.ms2'
    elif not randomize_precursor and randomize_fragments:
        output_file = ms2_file[:-4] + '_randFrag.ms2'
    else:
        output_file = ms2_file[:-4] + '_fixed.ms2'

    # read the ms2 file
    InFile = open(ms2_file, 'rU')
    ms2_lines = InFile.readlines()
    InFile.close()

    # collect info from the ms2 lines

    NEW_SPECTRUM = False
    for i in range(0, len(ms2_lines)):
        line = ms2_lines[i]
        info = line.split()
        if info[0] == 'S':
            NEW_SPECTRUM = True
            scan_num += 1
            s_line = line.split()
            s_line_index = i
        if NEW_SPECTRUM:
            if info[0] == 'Z':
                charge = float(info[1])
                MHplus = float(info[2])
                precursor = (MHplus - 1.007285 + charge * 1.007825) / charge
                new_s_line = s_line[0] + '\t' + s_line[1] + '\t' + s_line[2] + '\t' + str(precursor) + '\n'
                ms2_lines[s_line_index] = new_s_line
                NEW_SPECTRUM = False

    # fuck with the precursor
    if randomize_precursor:
        NEW_SPECTRUM = False
        for i in range(0, len(ms2_lines)):
            line = ms2_lines[i]
            info = line.split()
            if info[0] == 'S':
                NEW_SPECTRUM = True
                s_line = line.split()
                s_line_index = i
            if NEW_SPECTRUM:
                if info[0] == 'Z':
                    charge = float(info[1])
                    MHplus = float(info[2]) + random.choice(randRange)
                    precursor = (MHplus - 1.007285 + charge * 1.007825) / charge
                    new_s_line = s_line[0] + '\t' + s_line[1] + '\t' + s_line[2] + '\t' + str(precursor) + '\n'
                    new_z_line = info[0] + '\t' + str(int(charge)) + '\t' + str(MHplus) + '\n'
                    ms2_lines[s_line_index] = new_s_line
                    ms2_lines[i] = new_z_line
                    NEW_SPECTRUM = False

    # fuck with the fragments
    if randomize_fragments:
        NEW_SPECTRUM = False
        for i in range(0, len(ms2_lines)):
            line = ms2_lines[i]
            info = line.split()
            if info[0] != 'S' and info[0] != 'I' and info[0] != 'H' and info[0] != 'Z':
                if random.random() > unchanged_prob: #flip a coin to decide if to scramble
                    fragmz = float(info[0]) + random.choice(randRange)
                else:
                    fragmz = float(info[0])
                new_line = str(fragmz) + " " + info[1] + "\n"
                ms2_lines[i] = new_line

    # write to a tsv file
    OutFile = open(output_file, 'w')
    for line in ms2_lines:
        OutFile.write(line)
    OutFile.close()
    print "Number of ms2 scans " + ms2_file + " : ", scan_num

