__author__ = 'rj8'
"""This script compares de novo sequences with "correct" sequences obtained from a standard database search.  The
input file is a csv file containing the Novor sequences in the first column, the database sequence in the second
column, the Novor sequence scores in the third column, and the Novor amino acid scores in the fourth column.
An assessment is made whether the two sequences completely agree, as well as the fraction of peptide mass that
CIDentify would be able to align.  CIDentify can align two amino acids whose masses sum to a single amino acid
(query versus database), and two amino acids whose masses sum to two other amino acids (query versus database).
There are two output files (a) _output.txt has the two sequences (Novor and database), the sequence score and amino
acid scores, a column of True/False as to whether the two sequences are identical, and the last column is a fraction
(0 to 1) showing the fraction of peptide mass that can be aligned by CIDentify, and (b) -histogram.txt containing a
sequence score column, accumulative positives (from high to low), accumulative negatives (low to high), FPR, Precision,
and Recall. """

input_file = "humanCidLit_input.txt" #tsv file containing Novor, database, sequence score, and aaScore
output_file = input_file[:-4] + '_output.txt'
histogram_file = input_file[:-4] + '_histogram.txt'
CIDentify_fraction_threshold = 0.7 # above this number is considered correct
amino_acids = {'A':71, 'C':160, 'D':115, 'E':129, 'F':147, 'G':57, 'H':137, 'I':113, 'K':128, 'L':113, 'M':131,\
               'N':114, 'P':97, 'Q':128, 'R':156, 'S':87, 'T':101, 'V':99, 'W':186, 'Y':163}
number_not_compared = 0
number_where_mass_diff_more_than_1 = 0
number_where_mass_diff_more_than_2 = 0

def same_nominal_mass(seq1, seq2):
    seq1_mass = 0
    seq2_mass = 0
    global number_where_mass_diff_more_than_1, number_where_mass_diff_more_than_2
    for c in seq1:
        seq1_mass += amino_acids[c]
    for c in seq2:
        seq2_mass += amino_acids[c]
    if seq1_mass == seq2_mass:
        return True
    else:
        diff = abs(seq1_mass - seq2_mass)
        if diff > 1:
            number_where_mass_diff_more_than_1 += 1
        elif diff > 2:
            number_where_mass_diff_more_than_2 += 1
        return False

def calc_true_pos(pos):
    """True positivees are the numbers of positives at or above the score under consideration."""
    tp = [0] * len(pos)
    tp[len(pos) - 1] = pos[len(pos) - 1]
    for i in range(len(pos) - 2, 0, -1):
        tp[i] = pos[i] + tp[i + 1]
    return tp

def calc_false_pos(neg):
    """False positives are the numbers of negatives at or above the score under consideration"""
    fp = [0] * len(neg)
    fp[len(neg) - 1] = neg[len(neg) - 1]
    for i in range(len(neg) - 2, 0, -1):
        fp[i] = neg[i] + fp[i + 1]
    return fp

def calc_false_neg(pos):
    """False negatives are the numbers of positives below a given score"""
    fn = [0] * len(pos)
    fn[0] = 0
    for i in range(1, len(pos) - 1):
        fn[i] = fn[i - 1] + pos[i - 1]
    return fn

def calc_true_neg(neg):
    """True negatives are the number of negatives below a given score"""
    tn = [0] * len(neg)
    tn[0] = 0
    for i in range(1, len(neg) - 1):
        tn[i] = tn[i - 1] + neg[i - 1]
    return tn

def calc_false_pos_rate(fp, tn):
    """FPR = FP / (FP + TN).  Of all the things that are wrong, what fraction is called correct for a given score?"""
    fpr = []
    for i in range(0, len(fp) - 1):
        if float(fp[i]) + float(tn[i]) <= 0:
            fpr.append(0)
        else:
            fpr.append(float(fp[i]) / (float(fp[i]) + float(tn[i])))
    return fpr

def calc_precision(tp, fp):
    """Precision = TP / (TP + FP).  Of all the things that are called correct,
    how many are actually correct for a given score?"""
    precision = []
    for i in range(0, len(tp) - 1):
        if float(tp[i]) + float(fp[i]) <= 0:
            precision.append(0)
        else:
            precision.append(float(tp[i]) / (float(tp[i]) + float(fp[i])))
    return precision

def calc_recall(tp, fn):
    """Recall = TP / (TP + FN).  Of all the correct things, what fraction are called correct for a given score?"""
    recall = []
    for i in range(0, len(tp) - 1):
        if float(tp[i]) + float(fn[i]) <= 0:
            recall.append(0)
        else:
            recall.append(float(tp[i]) / (float(tp[i]) + float(fn[i])))
    return recall


#==================================================================================================================
#==================================================================================================================
# open the input file and read the lines into a list
InFile = open(input_file, 'rU')
info_list = InFile.readlines()
InFile.close()

# open the output file and give it the header line
OutFile = open(output_file, 'w')
OutFile.write('Novor\tDatabase\tScore\taa-score\tCompletely Correct\tCIDentify Correct\n')

# step through each pair of sequences to compare at the mass level
header_line = True #assume that there is a single line header to skip over
for line in info_list:
    if header_line:
        header_line = False
        continue
    info = line.split()
    if not same_nominal_mass(info[0], info[1]):
        number_not_compared += 1
        continue    # Make sure the two sequences have the same nominal mass, and skip if not
    novor_masses = [] # List of summed amino acid masses from the denovo sequence, starting at the N-terminus
    novor_mass_sum = 0 # Used to calculate novor_masses
    database_masses = [] #List of summed amino acid masses from the database sequence, starting at the N-terminus
    database_mass_sum = 0 # Used to calculate database_masses
    database_mass = [] # This is a list of the corresponding amino acid masses for the database sequence (not summed)
    database_map = [] # Indexed to the database_mass_sum, the values are the indices of matching novor_mass_sum
                      # values.
    for c in info[0]:
        novor_mass_sum += amino_acids[c]
        novor_masses.append(novor_mass_sum)
    for c in info[1]:
        database_mass_sum += amino_acids[c]
        database_masses.append(database_mass_sum)
        database_mass.append(amino_acids[c])
    for i in range(0, len(database_masses)):
        if database_masses[i] in novor_masses:
            database_map.append(novor_masses.index(database_masses[i]))
        else:
            database_map.append(-1)

    completely_correct = True # Initially assume that the two sequences are identical.
    CIDentify_correct_sum = 0 # The amount of sequence mass that CIDentify could align between the two sequences
    nomatch_count = 0 # Counts the number of consecutive values of -1 in database_map (ie, non-matches)
    for i in range(0, len(database_map)):
        if database_map[i] < 0:
            nomatch_count += 1
            completely_correct = False #-1 marks a point of disagreement between database and novor masses
            continue
        else:
            index_map_diff = i - database_map[i] #the diff between the database index and its map index
            if index_map_diff != 0:
                completely_correct = False #if the index and map don't align then it cannot be completely correct
            if nomatch_count == 0:
                CIDentify_correct_sum += database_mass[i]
            elif nomatch_count == 1:
                CIDentify_correct_sum += database_mass[i] + database_mass[i - 1] # two aa gaps are okay, but no more
            # re-align the two sequences via database_map
            for j in range(i, len(database_map)):
                if database_map[j] > -1:
                    database_map[j] += index_map_diff
            nomatch_count = 0 # reset the consecutive non-matching count
    # Calculate the fraction of sequence that CIDentify would consider to be correct
    if database_masses[len(database_masses) - 1] > 0:
        CIDentify_correct_fraction = float(CIDentify_correct_sum) / database_masses[len(database_masses) - 1]
    else:
        continue
    # prepare the output string and add to the OutFile
    outputStr = info[0] + '\t' + info[1] + '\t' + info[2] + '\t' + info[3] + '\t' + str(completely_correct) \
                + '\t' + str(CIDentify_correct_fraction) + '\n'
    OutFile.write(outputStr)

# close the OutFile
OutFile.close()

# Read the output file, extract the score and CIDentify_correct_fraction to make precision-recall curve
InFile = open(output_file, 'rU')

# sort based on the sequence score versus CIDentify threshold of being correct
positive = [0] * 101
negative = [0] * 101
header_line = True
for line in InFile:
    if header_line:
        header_line = False
        continue
    info = line.split()
    score = int(float(info[2]) + 0.5) #make the score an integer between 0 and 100
    fraction_correct = float(info[5])
    if fraction_correct > CIDentify_fraction_threshold:
        positive[score] += 1 #if correct, as far as CIDentify is concerned, then incremnt the index corresponding
                                #to the score
    else:
        negative[score] += 1

InFile.close()

# calculate TP, FP, FN, and TN
true_pos = calc_true_pos(positive)
false_pos = calc_false_pos(negative)
false_neg = calc_false_neg(positive)
true_neg = calc_true_neg(negative)

# calculate FPR, Precision, and Recall
false_pos_rate = calc_false_pos_rate(false_pos, true_neg)
precision = calc_precision(true_pos, false_pos)
recall = calc_recall(true_pos, false_neg)

OutFile = open(histogram_file, 'w')
OutFile.write('Sequence Score\tPositive\tNegative\tFPR\tPrecision\tRecall\n')
for i in range(0, len(positive) - 1):
    outputStr = str(i) + '\t' + str(positive[i]) +  '\t' + str(negative[i]) + '\t' + str(false_pos_rate[i]) + '\t'\
                + str(precision[i]) + '\t' + str(recall[i]) + '\n'
    OutFile.write(outputStr)
OutFile.close()

# Now look at the amino acid scores
InFile = open(output_file, 'rU')
positive = [0] * 101
negative = [0] * 101
header_line = True
for line in InFile:
    if header_line:
        header_line = False
        continue
    info = line.split()
    aa_scores = info[3].split('-')
    aa_scores = [int(i) for i in aa_scores]
    novor_aa = [str(i) for i in info[0]]
    database_aa = [str(i) for i in info[1]]
    if len(info[0]) == len(info[1]):
        for i in range(0 , len(aa_scores)):
            if database_aa[i] == 'I':
                database_aa[i] = 'L'
            if novor_aa[i] == database_aa[i]:
                positive[aa_scores[i]] += 1
            else:
                negative[aa_scores[i]] += 1

# calculate TP, FP, FN, and TN
true_pos = calc_true_pos(positive)
false_pos = calc_false_pos(negative)
false_neg = calc_false_neg(positive)
true_neg = calc_true_neg(negative)

# calculate FPR, Precision, and Recall
false_pos_rate = calc_false_pos_rate(false_pos, true_neg)
precision = calc_precision(true_pos, false_pos)
recall = calc_recall(true_pos, false_neg)

InFile.close()
OutFile = open(histogram_file, 'a')
OutFile.write('\n')
OutFile.write('Amino Acid Score\tPositive\tNegative\tFPR\tPrecision\tRecall\n')
for i in range(0, len(positive) - 1):
    outputStr = str(i) + '\t' + str(positive[i]) +  '\t' + str(negative[i]) + '\t' + str(false_pos_rate[i]) + '\t'\
                + str(precision[i]) + '\t' + str(recall[i]) + '\n'
    OutFile.write(outputStr)
OutFile.close()
print "The number of pairs that did not have the same nominal mass: ", str(number_not_compared)
print "Where", str(number_where_mass_diff_more_than_1), "were more than 1 Da difference, and", str(number_where_mass_diff_more_than_2), " were more than 2 Da."




